#include <string>
#include <unordered_map>
#include "cdltypes.hpp"
using namespace CDL;


static std::unordered_map<CGuild, CMessage> guild_invmsg_map;

void guild_clone(CMessage msg, CChannel channel, CDL::cmdargs&) {
    // Error callback
    auto fcb = [channel] () {
        channel->send("Failed to clone guild; try again later");
    };
    // Get guild
    channel->get_guild([fcb, msg, channel] (CGuild guild) {
        if (guild) {
            Guild::create(*guild, [fcb, msg, channel] (CGuild newguild) {
                if (newguild) {
                    BaseChannel invchannel({});
                    invchannel.name = "cpydone";
                    newguild->create_channel(invchannel, [fcb, msg, newguild, channel] (CChannel newchannel) {
                        if (newchannel) {
                            newchannel->create_invite([msg, newguild, channel] (std::optional<Invite> invite) {
                                channel->send("Clone: https://discord.gg/"+invite->code, [newguild, msg] (CMessage invmsg) {
                                    if (invmsg) {
                                        msg->remove();
                                        guild_invmsg_map[newguild] = invmsg;
                                    } else {
                                        newguild->remove();
                                    }
                                });
                            }, false, true, 1);
                        } else {
                            newguild->remove();
                            fcb();
                        }
                    });
                } else {
                    fcb();
                }
            });
        } else {
            fcb();
        }
    });
}

void channel_clone(CMessage msg, CChannel channel, CDL::cmdargs&) {
    // Check permissions
    if (!msg->member->has_perm(Permissions::MANAGE_CHANNELS)) {
        channel->send("Well oops, you can't do that here. :wink:");
        return;
    }
    // Error callback
    auto fcb = [channel] () {
        channel->send("Failed to clone channel");
    };
    // Get guild
    channel->get_guild([fcb, msg, channel] (CGuild guild) {
        if (guild) {
            guild->create_channel(*channel, [fcb, msg, channel] (CChannel newchannel) {
                if (newchannel) {
                    channel->send("Clone: "+newchannel->get_mention(), [fcb, msg, newchannel] (CMessage resmsg) {
                        if (resmsg) {
                            msg->remove();
                        }
                    });
                } else {
                    fcb();
                }
            });
        } else {
            fcb();
        }
    });
}

int main(int argc, char **argv) {
    register_command("guild", guild_clone, NO_ARGS);
    register_command("channel", channel_clone, NO_ARGS);
    register_command("help", [] (CMessage, CChannel channel, CDL::cmdargs&) {
        channel->send("Commands: `guild`, `channel`... That's it.");
    }, NO_ARGS);

    handlers::get_prefix = [] (CChannel, auto cb) {
        cb("clone!");
    };

    intents::guild_member_add.push_back([] (CMember member) {
        auto guild = member->guild;
        if (guild->is_owner) {
            auto res = guild_invmsg_map.find(guild);
            bool ress = res != guild_invmsg_map.end();
            if (ress) {
                guild_invmsg_map.erase(res);
                res->second->remove();
            }
            guild->owner_id = member->user_id;
            guild->commit([res, ress, guild] (const bool error) {
                if (error) {
                    guild->remove();
                }
            });
        }
    });
    intents::message_delete.push_back([] (const_CMessage msg) {
        for (const auto& [guild, invmsg] : guild_invmsg_map) {
            if (msg == invmsg) {
                guild->remove();
                break;
            }
        }
    });

    env.settings = {
        {"token", "NjUxNzY3ODkyOTYxOTE4OTc2.XeesJg.-JR4nqbW95Y-br_TEo-xkvXEttY"}
    };

    using namespace CDL::intent_vals;
    CDL::main(argc, argv, GUILDS | GUILD_MESSAGES | GUILD_MEMBERS);
}
